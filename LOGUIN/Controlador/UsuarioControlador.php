<?php
 require_once('../conexion.php');

require_once("../Modelo/Usuario.php");
require_once("../Modelo/CrudUsuario.php");


$Usuario = new Usuario();
$CrudUsuario = new CrudUsuario();

if(isset($_POST["login"])){


    $Usuario->setNombreUsuario($_POST['NombreUsuario']);
    $Usuario->setContrasena($_POST['contrasena']);

    $Usuario = $CrudUsuario->ValidarAcceso($Usuario);

    if($Usuario->getExiste()==1){
        header("Location:../Menu.php");
    }
    else
    {
        ?>

            <script>
                alert("usuario y/o contraseña incorrecto ");
                document.location.href="../login.php";
            </script>

        <?php
    }
   

}
else
{
    header("Location:../login.php");
}

?>

