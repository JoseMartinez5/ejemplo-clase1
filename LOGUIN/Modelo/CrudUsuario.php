<?php
 require_once('../conexion.php');
 require_once('../Controlador/UsuarioControlador.php');
 

 class CrudUsuario{

    public function __construct(){}

    public function ValidarAcceso($Usuario){

         $Db = Db::Conectar();// clase conexion
           $SQL = $Db->prepare('SELECT * FROM datos where NombreUsuario=:NombreUsuario and Contrasena=:Contrasena and IdEstado=1'); 

           $SQL->bindValue('NombreUsuario',$Usuario->getNombreUsuario());

           $SQL->bindValue('Contrasena',$Usuario->getContrasena());
           $SQL->execute();

            $MyUsuario = new Usuario();
    
           if($SQL->rowCount() > 0){

            $Usuario = $SQL->fetch();
           
            $MyUsuario->setIdUsuario($Usuario['IdUsuario']);

             $MyUsuario->setNombreUsuario($Usuario['NombreUsuario']);

             $MyUsuario->setIdRol($Usuario['IdRol']);
              $MyUsuario->setExiste(1);


           }else{
                $MyUsuario->setExiste(0);
           }

           return $MyUsuario;
       
    
                
    }


 }



?>