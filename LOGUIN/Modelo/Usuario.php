<?php



    class Usuario{

    private $IdUsuario;
    private $NombreUsuario;
    private $Contrasena;
    private $IdRol;
    private $IdEstado;
    private $Existe;

    // contructor

    public function __contruct(){

    }

    // set y get
    // Id
    public function setIdUsuario($IdUsuario){
         $this ->IdUsuario = $IdUsuario;
    }
    
    public function getIdUsuario(){
        return $this ->IdUsuario;
    }

   
    // nombre usuario
    public function setNombreUsuario($NombreUsuario){
         $this ->NombreUsuario = $NombreUsuario;
    }
    
    public function getNombreUsuario(){
        return $this ->NombreUsuario;
    }

    // contraseña
    public function setContrasena($Contrasena){
         $this ->Contrasena = $Contrasena;
    }

    public function getContrasena(){
        return $this ->Contrasena;
    }

     // Rol
    public function setIdRol($IdRol){
         $this ->IdRol = $IdRol;
    }
    public function getIdRol(){
        return $this ->IdRol;
    }

    // Estado
    public function setIdEstado($IdEstado){
         $this ->IdEstado = $IdEstado;
    }
    public function getIdEstado(){
        return $this ->IdEstado;
    }

    // existe
    public function setExiste($Existe){
         $this ->Existe = $Existe;
    }
    public function getExiste(){
        return $this ->Existe;
    }
}
/*
// testear la Clase 
$Usuario = new Usuario();

$Usuario->setIdUsuario(8888);
$Usuario->setNombreUsuario("jose david");
$Usuario->setContrasena(12345);
$Usuario->setIdRol("ADMIN");
$Usuario->setIdEstado("Activo");

echo " Id :".$Usuario->getIdUsuario();
echo "<br>";

echo " nombre_u :".$Usuario->getNombreUsuario();
echo "<br>";

echo "contraseña :".$Usuario->getContrasena();
echo "<br>";


echo "Rol :".$Usuario->getIdRol();
echo "<br>";


echo "Estado:".$Usuario->getIdEstado();
echo "<br>";
*/


?>