<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="../css/MyStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
    <title>Registro</title>
</head>

<body>

    <header class="header">

        <nav class="nav">

            <i class="i">
                <img src="../img/Logo.png" width="350px" alt="">
                <div class="menu">
                    <div class="ingresar">
                        <a href="../login.php">Login</a>
                        <a href="registro.php">Registrarse</a>
                 
            </i>


        </nav>


    </header>
        <div class="row">
                    <h5 align="center"> Crear Cuenta</h5>
            <form  action="../Controlador/UsuarioControlador.php" name="frmRegistro" id="frmRegistro" method="POST">
                

                <div class="row">

                    <div class="input-field col s6">
                        
                        <input name="NombreUsuario" id="NombreUsuario" type="text" class="validate">
                        <label for="NombreUsuario">Nombre</label>
                    </div>
                        <br><br>

                    <div class="input-field col s6">
                   
                        <input name="contrasena" id="contrasena" type="password" class="validate">
                        <label for="contrasena">Contraseña</label>
                    </div>
                    <br><br>
                    <div class="input-field col s6">
                      
                        <input name="IdRol" id="IdRol" type="text" class="validate">
                        <label for="IdRol">Rol</label>
                    </div>
                      <br><br>
                    <div class="input-field col s6">
                       
                        <input name="IdEstado" id="IdEstado" type="text" class="validate">
                        <label for="IdEstado">Estado</label>
                    </div>
                      <br><br>
                   
                      <br><br>
                </div>
                     <input type="hidden" name="registar">
              
                    <button type="submit" name="btnRegistar" name="btnRegistar">Ingresar</button>
                    
               

            </form>

        </div>




    <footer>


    </footer>

</body>
<script src="js/funciones.js"></script>
<!-- Compiled and minified JavaScript -->
</html>